package calend;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class calendar {

	    public static void main(String[] args) throws InterruptedException {
	    	System.out.println("Give me a date in ISO format YYYY-MM-DD");
	    	
	    	//Scanner scanner = new Scanner(System.in);
			//String datee = scanner.nextLine();
			String datee = "2021-04-28";
			String[] ddsp = datee.split("-");

			int den = Integer.parseInt(ddsp[2]);
			int mesiac = Integer.parseInt(ddsp[1]);
			int rok = Integer.parseInt(ddsp[0]);
	        LocalDate d = LocalDate.of(rok, mesiac, den);

	        System.out.println("Zadan� d�tum: " + d.getMonth() + " " + d.getYear());
	        System.out.println("Vas kalendar:");

	        String[] week = {
	            "Pon", "Uto", "Str",
	            "�tv", "Pia", "Sob", "Ned"
	        };

	        for (int i = 0; i < week.length; i++) {
	            System.out.print("" + week[i] + " ");
	        }

	        //spravit list cisel ktore budeme pisat
	        List<Integer> dni = new ArrayList<Integer>();

	        int dniTu = d.getMonth().maxLength();

	        if (nieJePriestupnyFebruar(d)) {
	            dniTu--;
	        }

	        LocalDate dprvy = LocalDate.of(rok, mesiac, 1);
	        int trebaZMinMesiaca = dprvy.getDayOfWeek().getValue() - 1;

	        int mesiacPred = mesiac - 1;
	        if (mesiacPred == 0) {
	            mesiacPred = 12;
	        }

	        LocalDate dd = LocalDate.of(rok, mesiacPred, den);
	        int dniPredMax = dd.getMonth().maxLength();
	        if (nieJePriestupnyFebruar(dd)) {
	            dniPredMax--;
	        }

	        dniPredMax = dniPredMax - trebaZMinMesiaca + 1;

	        for (int i = 0; i < trebaZMinMesiaca; i++) {
	            dni.add(dniPredMax);
	            dniPredMax++;
	        }

	        for (int i = 0; i < dniTu; i++) {
	            dni.add((i + 1));
	        }

	        int dokopyMam = trebaZMinMesiaca + dniTu;

	        int doplnit = 7 - (dokopyMam % 7);

	        for (int i = 0; i < doplnit; i++) {
	            dni.add((i + 1));
	        }
	        for (int i = 0; i < dni.size(); i++) {
	            if (i % 7 == 0) {
	                System.out.println("");
	            }
	            System.out.printf("%2d  ", dni.get(i));
	        }

	        ///////////////
	        System.out.println("");

	        ScheduledExecutorService exe = Executors.newSingleThreadScheduledExecutor();
	        exe.scheduleAtFixedRate(new Runnable() {
	            public void run() {
	                LocalTime d = LocalTime.now();
	                System.out.print("\r" + d);
	            }
	        }, 0, 1, TimeUnit.SECONDS);

	        if (exe == null) {
	            exe.shutdown();
	        }

	    }

	    public static boolean nieJePriestupnyFebruar(LocalDate d) {
	        if (d.getMonthValue() != 2) {
	            return false;
	        }
	        if (d.getYear() % 4 == 0) {
	            return false;
	        }
	        return true;

	    }

	}

